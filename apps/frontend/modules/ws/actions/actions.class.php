<?php

/**
 * ws actions.
 *
 * @package    ws_siabuc9
 * @subpackage ws
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class wsActions extends sfActions
{
   
  /**
   * 
   * @WSMethod(name='RegUsuario', webservice='Siabuc9')
   * 
   * @param string $NumCuenta Numero de cuenta
   * @param string $Nombre Nombre del usuario
   * @param integer $Escuela Identificador de escuela
   * @param string $Correo Correo del usuario
   * @param string $Domicilio Dirección del usuario
   * @param string $Colonia Departamento/Barrio del usuario
   * @param string $Ciudad Ciudad del usuario
   * @param string $CodPos Código postal del usuario
   * @param string $Telefono Teléfono del usuario
   * @param integer $NumGrupo Identificador de grupo del usuario
   * @param string $VigenciaIni Fecha de vigencia inicial del usuario (Formato AAAA-MM-DD)
   * @param string $VigenciaFin Fecha de vigencia final del usuario (Formato AAAA-MM-DD)
   * @param string $Nip Numero de identificacion
   * @param string $Notas Notas
   * 
   * @return string guardado o no_guardado
   */
  public function executeRegUsuario(sfWebRequest $request){
          
      try{
            $usuario= new Usuario();
            $usuario->setNoCuenta($request->getParameter("NumCuenta"));
            $usuario->setNombre(trim($request->getParameter("Nombre")));
            $usuario->setIdescuela($request->getParameter("Escuela"));
            $usuario->setCorreo($request->getParameter("Correo"));
            $usuario->setDomicilio($request->getParameter("Domicilio"));
            $usuario->setColonia($request->getParameter("Colonia"));
            $usuario->setCiudadEstado($request->getParameter("Ciudad"));
            $usuario->setCodPos($request->getParameter("CodPos"));
            $usuario->setTelefono($request->getParameter("Telefono"));
            $usuario->setNoGrupo($request->getParameter("NumGrupo"));
            $usuario->setInicioVigencia($request->getParameter("VigenciaIni"));
            $usuario->setFinVigencia($request->getParameter("VigenciaFin"));
            $usuario->setNip($request->getParameter("Nip"));
            $usuario->setNotas($request->getParameter("Notas"));
            $usuario->setAnalista("SW_SIABUC9");
            $usuario->save();
        }
        catch(Exception $e){
            
            throw new sfException($e->getMessage());
            $this->result="no_guardado";
            return sfView::ERROR;
        }
        
        $this->result="guardado";
        return sfView::SUCCESS; 
  }
  
  /**
   * 
   * @WSMethod(name='ActUsuario', webservice='Siabuc9')
   * 
   * @param string $NuevoNumCuenta Nuevo Numero de cuenta
   * @param string $ActualNumCuenta Numero de cuenta
   * @param string $Nombre Nombre del usuario
   * @param integer $Escuela Identificador de escuela
   * @param string $Correo Correo del usuario
   * @param string $Domicilio Dirección del usuario
   * @param string $Colonia Departamento/Barrio del usuario
   * @param string $Ciudad Ciudad del usuario
   * @param string $CodPos Código postal del usuario
   * @param string $Telefono Teléfono del usuario
   * @param integer $NumGrupo Identificador de grupo del usuario
   * @param string $VigenciaIni Fecha de vigencia inicial del usuario (Formato AAAA-MM-DD)
   * @param string $VigenciaFin Fecha de vigencia final del usuario (Formato AAAA-MM-DD)
   * @param string $Nip Numero de identificacion
   * @param string $Notas Notas
   * 
   * @return string guardado o no_guardado
   */
  public function executeActUsuario(sfWebRequest $request){
          
      try{

	    $q=Doctrine_Query::create()
            ->update("Usuario u")
            ->set("u.no_cuenta","?",$request->getParameter("NuevoNumCuenta"))
            ->set("u.nombre","?",trim($request->getParameter("Nombre")))
            ->set("u.no_grupo","?",$request->getParameter("NumGrupo"))
            ->set("u.idescuela","?",$request->getParameter("Escuela"))
            ->set("u.correo","?",$request->getParameter("Correo"))
            ->set("u.domicilio","?",$request->getParameter("Domicilio"))
            ->set("u.colonia","?",$request->getParameter("Colonia"))
            ->set("u.ciudad_estado","?",$request->getParameter("Ciudad"))
            ->set("u.cod_pos","?",$request->getParameter("CodPos"))
            ->set("u.telefono","?",$request->getParameter("Telefono"))
            ->set("u.inicio_vigencia","?",$request->getParameter("VigenciaIni"))
            ->set("u.fin_vigencia","?",$request->getParameter("VigenciaFin"))
            ->set("u.nip","?",$request->getParameter("Nip"))
            ->set("u.notas","?",$request->getParameter("Notas"))
            ->where("u.no_cuenta = ?",$request->getParameter("ActualNumCuenta"))
            ->execute();
            
            $this->result='guardado';
            return sfView::SUCCESS; 

        }
        catch(Exception $e){
            
            throw new sfException($e->getMessage());
            $this->result="no_guardado";
            return sfView::ERROR;
        }
        
  }
  
  /**
   * 
   * @WSMethod(name='EliminaUsuario', webservice='Siabuc9')
   * 
   * @param string $NoCta Numero de cuenta
   * @param string $NiP Nombre del usuario
   * 
   * @return string eliminado o no_eliminado
   */
  public function executeEliminaUsuario(sfWebRequest $request){
        
        try{  
            $usuario=Doctrine_Core::getTable("Usuario")
                        ->createQuery("u")
                        ->where("u.no_cuenta = ?",$request->getParameter("NoCta"))
                        ->orWhere("u.nip = ?",$request->getParameter("NiP"))
                        ->execute();
                        
            if($usuario)
                $usuario->delete();
            else
                throw new sfException("no existe el usuario");
        }
        catch(Exception $e){
            throw new sfException($e->getMessage());
            $this->result="no_eliminado";
            return sfView::ERROR;
        }
        
        $this->result="eliminado";
        return sfView::SUCCESS; 
            
  }
  
  /**
   * 
   * @WSMethod(name='ValidaUsuario', webservice='Siabuc9')
   * 
   * @param string $NoCta Numero de cuenta
   * @param string $NiP Nombre del usuario
   * 
   * @return integer 1-usuario registrado, 2-no existe el usuario, 3-nip incorrecto
   */
  public function executeValidaUsuario(sfWebRequest $request){
        
	try{  
        $usuario=Doctrine_Core::getTable("Usuario")
                    ->createQuery("u")
                    ->where("u.no_cuenta = ?",$request->getParameter("NoCta"))
                    ->orWhere("u.nip = ?",$request->getParameter("NiP"))
                    ->execute();
                       
        if(count($usuario)>0){
         $this->result=1;
         return sfView::SUCCESS;
         }
         else{
            $this->result=2;
            return sfView::ERROR;
         }
	}
catch(Exception $e){
	$this->result=-1;
}
      
      
  }
  
  /**
   * 
   * @WSMethod(name='ListarEscuelas', webservice='Siabuc9')
   * 
   * @param string $IdBiblio Identificador de biblioteca
   * 
   * @return EscuelaObjeto[] Arreglo con los elementos IdEscuela (integer) y Nombre (string)
   */
  public function executeListarEscuelas(sfWebRequest $request){

        $escuelas=array();
                
        foreach(Doctrine_Core::getTable("Escuela")->createQuery("e")->where("e.idbiblioteca = ?",$request->getParameter("IdBiblio"))->execute() as $g){
            $bean=new EscuelaObjeto();
            $bean->setIdEscuela($g->getIdescuela());
            $bean->setNombre($g->getNombre());
            $escuelas[]=$bean;
        }
                       
        $this->result=$escuelas;
        return sfView::SUCCESS;
  }
  
  /**
   * 
   * @WSMethod(name='ListarGrupos', webservice='Siabuc9')
   * 
   * 
   * @return GrupoObjeto[] Arreglo con los elementos Número Grupo (integer) y Descripción (string)
   */
  public function executeListarGrupos(sfWebRequest $request){

        $grupos=array();
        foreach(Doctrine_Core::getTable("Grupo")->createQuery()->execute() as $g){
            $bean=new GrupoObjeto();
            $bean->setNoGrupo($g->getNoGrupo());
            $bean->setDescripcion($g->getDescripcion());
            $grupos[]=$bean;
        }
        //$this->result=$grupos;
        $this->result=$grupos;
        return sfView::SUCCESS;
  }
  
  /**
   * 
   * @WSMethod(name='ListarBibliotecas', webservice='Siabuc9')
   * 
   * 
   * @return BibliotecaObjeto[] Arreglo con los elementos Id Biblioteca (integer) y Nombre (string)
   */
  public function executeListarBibliotecas(sfWebRequest $request){

        $grupos=array();
        foreach(Doctrine_Core::getTable("Biblioteca")->createQuery()->execute() as $g){
            $bean=new BibliotecaObjeto();
            $bean->setIdBiblioteca($g->getIdBiblioteca());
            $bean->setNombre($g->getNombre());
            $grupos[]=$bean;
        }
        //$this->result=$grupos;
        $this->result=$grupos;
        return sfView::SUCCESS;
  }
  
  /**
   * 
   * @WSMethod(name='UsuarioPazySalvo', webservice='Siabuc9')
   * 
   * @param string $NoCta Numero de cuenta
   * 
   * @return integer Establece si el usuario está a paz y salvo o no con la biblioteca: 0 no esta a paz y salvo, 1 si está a paz y salvo, -1 error
   */
  public function executeUsuarioPazySalvo(sfWebRequest $request){
      
      try{
          $prestamos=Doctrine_Core::getTable("Prestamo")
                    ->createQuery("p")
                    ->where("p.no_cuenta = ?",$request->getParameter("NoCta"))
                    ->count();
      
          if($prestamos>0){
              $this->result=0;
              return sfView::SUCCESS;
          }
              
        }
        catch (Exception $e){
             throw new sfException($e->getMessage());
                    $this->result=-1;
                    return sfView::ERROR;
        }
      
      try{
          
          $bloqueos=Doctrine_Core::getTable("Bloqueo")
                    ->createQuery("b")
                    ->where("b.no_cuenta = ?",$request->getParameter("NoCta"))
                    ->count();
          
          if($bloqueos>0){
              $this->result=0;
              return sfView::SUCCESS;
          }
      }
      catch (Exception $e){
             throw new sfException($e->getMessage());
                    $this->result=-1;
                    return sfView::ERROR;
        }
      
      
      $this->result=1;
      return sfView::SUCCESS;
  }
}
