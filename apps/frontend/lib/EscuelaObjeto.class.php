<?php
/**
 * @PropertyStrategy('ckBeanPropertyStrategy')
 */
class EscuelaObjeto {
    private $_IdEscuela;
    private $_Nombre;
    
           
   /**
   * llave
   *
   * @return string llave
   */    
    public function getIdEscuela(){
        return $this->_IdEscuela;
    }
    
   /**
   * valor
   *
   * @return string valor
   */ 
    public function getNombre(){
        return $this->_Nombre;
    }
    
   /**
   * valor
   *
   * @param string valor
   */ 
    public function setNombre($Nombre){
        $this->_Nombre=$Nombre;
    }
    
    /**
   * llave
   *
   * @param string llave
   */ 
    public function setIdEscuela($IdEscuela){
        $this->_IdEscuela=$IdEscuela;
    }
    
    
    
}

?>
