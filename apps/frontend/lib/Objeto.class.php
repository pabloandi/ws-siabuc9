<?php
/**
 * @PropertyStrategy('ckBeanPropertyStrategy')
 */
class Objeto {
    private $_key;
    private $_value;
    
           
   /**
   * llave
   *
   * @return string llave
   */    
    public function getKey(){
        return $this->_key;
    }
    
   /**
   * valor
   *
   * @return string valor
   */ 
    public function getValue(){
        return $this->_value;
    }
    
   /**
   * valor
   *
   * @param string valor
   */ 
    public function setValue($value){
        $this->_value=$value;
    }
    
    /**
   * llave
   *
   * @param string llave
   */ 
    public function setKey($key){
        $this->_key=$key;
    }
    
    
    
}

?>
