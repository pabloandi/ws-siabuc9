<?php
/**
 * @PropertyStrategy('ckBeanPropertyStrategy')
 */
class GrupoObjeto {
    private $_NoGrupo;
    private $_Descripcion;
    
           
   /**
   * llave
   *
   * @return string llave
   */    
    public function getNoGrupo(){
        return $this->_NoGrupo;
    }
    
   /**
   * valor
   *
   * @return string valor
   */ 
    public function getDescripcion(){
        return $this->_Descripcion;
    }
    
   /**
   * valor
   *
   * @param string valor
   */ 
    public function setDescripcion($Descripcion){
        $this->_Descripcion=$Descripcion;
    }
    
    /**
   * llave
   *
   * @param string llave
   */ 
    public function setNoGrupo($NoGrupo){
        $this->_NoGrupo=$NoGrupo;
    }
    
    
    
}

?>
