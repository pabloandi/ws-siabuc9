<?php
/**
 * @PropertyStrategy('ckBeanPropertyStrategy')
 */
class BibliotecaObjeto {
    private $_IdBiblioteca;
    private $_Nombre;
    
           
   /**
   * llave
   *
   * @return string llave
   */    
    public function getIdBiblioteca(){
        return $this->_IdBiblioteca;
    }
    
   /**
   * valor
   *
   * @return string valor
   */ 
    public function getNombre(){
        return $this->_Nombre;
    }
    
   /**
   * valor
   *
   * @param string valor
   */ 
    public function setNombre($Nombre){
        $this->_Nombre=$Nombre;
    }
    
    /**
   * llave
   *
   * @param string llave
   */ 
    public function setIdBiblioteca($IdBiblioteca){
        $this->_IdBiblioteca=$IdBiblioteca;
    }
    
    
    
}

?>
