<?php

/**
 * This is an auto-generated SoapHandler. All changes to this file will be overwritten.
 */
class BaseSiabuc9Handler extends ckSoapHandler
{
  public function RegUsuario($NumCuenta, $Nombre, $Escuela, $Correo, $Domicilio, $Colonia, $Ciudad, $CodPos, $Telefono, $NumGrupo, $VigenciaIni, $VigenciaFin, $Nip, $Notas)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('ws', 'regUsuario', array($NumCuenta, $Nombre, $Escuela, $Correo, $Domicilio, $Colonia, $Ciudad, $CodPos, $Telefono, $NumGrupo, $VigenciaIni, $VigenciaFin, $Nip, $Notas));
  }

  public function ActUsuario($NuevoNumCuenta, $ActualNumCuenta, $Nombre, $Escuela, $Correo, $Domicilio, $Colonia, $Ciudad, $CodPos, $Telefono, $NumGrupo, $VigenciaIni, $VigenciaFin, $Nip, $Notas)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('ws', 'actUsuario', array($NuevoNumCuenta, $ActualNumCuenta, $Nombre, $Escuela, $Correo, $Domicilio, $Colonia, $Ciudad, $CodPos, $Telefono, $NumGrupo, $VigenciaIni, $VigenciaFin, $Nip, $Notas));
  }

  public function EliminaUsuario($NoCta, $NiP)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('ws', 'eliminaUsuario', array($NoCta, $NiP));
  }

  public function ValidaUsuario($NoCta, $NiP)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('ws', 'validaUsuario', array($NoCta, $NiP));
  }

  public function ListarEscuelas($IdBiblio)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('ws', 'listarEscuelas', array($IdBiblio));
  }

  public function ListarGrupos()
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('ws', 'listarGrupos', array());
  }

  public function ListarBibliotecas()
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('ws', 'listarBibliotecas', array());
  }

  public function UsuarioPazySalvo($NoCta)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('ws', 'usuarioPazySalvo', array($NoCta));
  }
}