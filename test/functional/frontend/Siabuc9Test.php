<?php

$app   = 'frontend';
$debug = true;
 
include_once(dirname(__FILE__).'/../../bootstrap/soaptest.php');
 
$c = new ckTestSoapClient();
 
// test executeRegUsuario

$c->RegUsuario('123456','Pablo Andrés Díaz',9999,'pabloandi@gmail.com','Cra 22A # 12A-12','Valle del Cauca','Cali','0','3043376236',11,'2011-05-24','2014-10-10')    // call the action
  ->isFaultEmpty()         // check there are no errors
  ->isType('', 'string')   // check the result type is double
  ->is('', 'guardado');            // check the result value is 10

$c->ValidaUsuario('123456')
  ->isFaultEmpty()
  ->isType('','integer')
  ->is('',2);

?>
