<?php

/**
 * BasePrestamo
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $idprestamo
 * @property integer $idbiblioteca
 * @property string $no_adqui
 * @property string $no_cuenta
 * @property integer $idescuela
 * @property date $f_prestamo
 * @property date $f_entrega
 * @property integer $renovaciones
 * @property string $analisis_presto
 * @property string $tipo_prestamo
 * @property string $usuario
 * @property string $titulo
 * @property string $autor
 * @property string $clasificacion
 * @property date $f_devolucion
 * @property integer $tipo_material
 * @property integer $folio_renovacion
 * @property string $analista_renovo
 * @property date $f_alerta
 * @property integer $alertas
 * 
 * @method integer  getIdprestamo()       Returns the current record's "idprestamo" value
 * @method integer  getIdbiblioteca()     Returns the current record's "idbiblioteca" value
 * @method string   getNoAdqui()          Returns the current record's "no_adqui" value
 * @method string   getNoCuenta()         Returns the current record's "no_cuenta" value
 * @method integer  getIdescuela()        Returns the current record's "idescuela" value
 * @method date     getFPrestamo()        Returns the current record's "f_prestamo" value
 * @method date     getFEntrega()         Returns the current record's "f_entrega" value
 * @method integer  getRenovaciones()     Returns the current record's "renovaciones" value
 * @method string   getAnalisisPresto()   Returns the current record's "analisis_presto" value
 * @method string   getTipoPrestamo()     Returns the current record's "tipo_prestamo" value
 * @method string   getUsuario()          Returns the current record's "usuario" value
 * @method string   getTitulo()           Returns the current record's "titulo" value
 * @method string   getAutor()            Returns the current record's "autor" value
 * @method string   getClasificacion()    Returns the current record's "clasificacion" value
 * @method date     getFDevolucion()      Returns the current record's "f_devolucion" value
 * @method integer  getTipoMaterial()     Returns the current record's "tipo_material" value
 * @method integer  getFolioRenovacion()  Returns the current record's "folio_renovacion" value
 * @method string   getAnalistaRenovo()   Returns the current record's "analista_renovo" value
 * @method date     getFAlerta()          Returns the current record's "f_alerta" value
 * @method integer  getAlertas()          Returns the current record's "alertas" value
 * @method Prestamo setIdprestamo()       Sets the current record's "idprestamo" value
 * @method Prestamo setIdbiblioteca()     Sets the current record's "idbiblioteca" value
 * @method Prestamo setNoAdqui()          Sets the current record's "no_adqui" value
 * @method Prestamo setNoCuenta()         Sets the current record's "no_cuenta" value
 * @method Prestamo setIdescuela()        Sets the current record's "idescuela" value
 * @method Prestamo setFPrestamo()        Sets the current record's "f_prestamo" value
 * @method Prestamo setFEntrega()         Sets the current record's "f_entrega" value
 * @method Prestamo setRenovaciones()     Sets the current record's "renovaciones" value
 * @method Prestamo setAnalisisPresto()   Sets the current record's "analisis_presto" value
 * @method Prestamo setTipoPrestamo()     Sets the current record's "tipo_prestamo" value
 * @method Prestamo setUsuario()          Sets the current record's "usuario" value
 * @method Prestamo setTitulo()           Sets the current record's "titulo" value
 * @method Prestamo setAutor()            Sets the current record's "autor" value
 * @method Prestamo setClasificacion()    Sets the current record's "clasificacion" value
 * @method Prestamo setFDevolucion()      Sets the current record's "f_devolucion" value
 * @method Prestamo setTipoMaterial()     Sets the current record's "tipo_material" value
 * @method Prestamo setFolioRenovacion()  Sets the current record's "folio_renovacion" value
 * @method Prestamo setAnalistaRenovo()   Sets the current record's "analista_renovo" value
 * @method Prestamo setFAlerta()          Sets the current record's "f_alerta" value
 * @method Prestamo setAlertas()          Sets the current record's "alertas" value
 * 
 * @package    ws_siabuc9
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BasePrestamo extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('prestamo.prestamos');
        $this->hasColumn('idprestamo', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             ));
        $this->hasColumn('idbiblioteca', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('no_adqui', 'string', 20, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 20,
             ));
        $this->hasColumn('no_cuenta', 'string', 20, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 20,
             ));
        $this->hasColumn('idescuela', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('f_prestamo', 'date', null, array(
             'type' => 'date',
             'notnull' => true,
             ));
        $this->hasColumn('f_entrega', 'date', null, array(
             'type' => 'date',
             'notnull' => true,
             ));
        $this->hasColumn('renovaciones', 'integer', null, array(
             'type' => 'integer',
             'default' => 0,
             ));
        $this->hasColumn('analisis_presto', 'string', 10, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 10,
             ));
        $this->hasColumn('tipo_prestamo', 'string', 3, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 3,
             ));
        $this->hasColumn('usuario', 'string', 50, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 50,
             ));
        $this->hasColumn('titulo', 'string', 150, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 150,
             ));
        $this->hasColumn('autor', 'string', 100, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 100,
             ));
        $this->hasColumn('clasificacion', 'string', 50, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 50,
             ));
        $this->hasColumn('f_devolucion', 'date', null, array(
             'type' => 'date',
             ));
        $this->hasColumn('tipo_material', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('folio_renovacion', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('analista_renovo', 'string', 10, array(
             'type' => 'string',
             'length' => 10,
             ));
        $this->hasColumn('f_alerta', 'date', null, array(
             'type' => 'date',
             ));
        $this->hasColumn('alertas', 'integer', null, array(
             'type' => 'integer',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        
    }
}